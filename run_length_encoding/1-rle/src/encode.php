<?php

function encode_rle($str)
{
    $i = 0;
    $comptr=1;
    $l=strlen($str);
    $end = '';
    while ($str==""){
        return "";
    }
    while (!ctype_alpha($str)){
        return "$$$";
    }
    while ($i<$l)
    {
     while(($i+1<$l) && ($str[$i]==$str[$i+1])){
         $comptr++;
         $i++;
         }
         $end.=$comptr.$str[$i];
     $comptr=1;
     $i++;
    }
    return $end;
}