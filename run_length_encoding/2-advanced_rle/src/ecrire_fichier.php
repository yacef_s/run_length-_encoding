<?php

function ecrire_file(string $output_path, string $encode) {

    $ecrire_fichier = fopen($output_path, 'w') or die('Cannot open file:  '.$output_path);
    
    if (substr($output_path, -4) == ".bmp" || substr($output_path, -4) == ".BMP") {
        $encode = chr($encode);
    }
    
    fwrite($ecrire_fichier, $encode);
    fclose($ecrire_fichier);
}

?>