<?php
function encode_advanced_rle(string $input_path, string $output_path) {

    $i = 0;
    $valeur_repete = 1;
    $valeur_non_repete = 0;
    $compression = "";
    $mot = "";
    $fichier_lu = file_get_contents($input_path);
    $taille_fichier = strlen($fichier_lu);

        
        if(empty($fichier_lu)) {
                               file_put_contents($output_path,$compression);
                               return 0;
        }
        if(! isset($fichier_lu[$i+1])) {
                                    $compression .= chr(0).chr(1).$fichier_lu[$i];
        }
        while ($i < $taille_fichier && isset($fichier_lu[$i+1])) { 
            if ($fichier_lu[$i] == $fichier_lu[$i+1]) {   
                if ($valeur_repete < 255) 
                    $valeur_repete++;   
                else { 
                    $compression .=  chr($valeur_repete) . $fichier_lu[$i]; 
                    $valeur_repete = 1;
                }
                if ($valeur_non_repete != 0){ 
                    $compression .=  chr(0) . chr($valeur_non_repete) . $mot;
                    $mot = ""; 
                } 
                $valeur_non_repete = 0; 
            }
            else {
                    if ($valeur_repete > 1)
                       $compression .=  chr($valeur_repete) . $fichier_lu[$i];
                    else {
                    if ($valeur_non_repete == 255) {
                        $compression .=  chr(0) . chr($valeur_non_repete) . $mot; 
                        $valeur_non_repete = 0; 
                        $mot = ""; 
                    }
                    $mot .= $fichier_lu[$i];
                    $valeur_non_repete++;
                }
                $valeur_repete = 1;
                }
            $i++;
        }
        if ($valeur_repete > 1)
                    $compression .=  chr($valeur_repete) . $fichier_lu[$i];

        if($valeur_non_repete != 0)
            $compression .= chr(0) . chr($valeur_non_repete + 1) . $mot.$fichier_lu[$i];
        file_put_contents($output_path, $compression);
        return 0;

}


?>