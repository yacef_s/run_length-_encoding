<?php

    function lire_file(string $input_path) {
             $lire_fichier = fopen($input_path, 'r');
             $tmp = fread($lire_fichier, filesize($input_path));
 
             if (filesize($input_path) < 4) {
                return -1;
             }
             
             if (substr($input_path, -4) == ".bmp" || substr($input_path, -4) == ".BMP") {
                $tmp = chr($tmp);
             }
             fclose($lire_fichier);
             return ($tmp);
    }

?>
